import React, { useState } from 'react';
import axios from "axios";
import { NavLink, useNavigate } from 'react-router-dom';

const Login = () => {

    let navigate = useNavigate();

    const [loginError, setLoginError] = useState('');

    const [formData, setFormData] = useState({
        email: '',
        password: '',
    });

    const { email, password } = formData;

    const onInputChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const onSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.post("http://localhost:8080/api/candy-login/login", formData);
            if (response.status === 200) {
                sessionStorage.setItem('namePersonal', email); // Set namePersonal in session storage
                navigate("/");
                console.log(response);
              }
        } catch (error) {
            console.log(
                "errore"
            );
            if (error.response && error.response.status === 401) {
                setLoginError("Invalid email or password")
                console.log(setLoginError)
            } else {
                setLoginError("An error occurred")
                console.log(setLoginError)
            }
        }
    }

    return (
        <>
            <section className="text-center text-lg-start">
                <style>
                    {`.cascading-right {
                    margin-right: -50px;
                }
                
                @media (max-width: 991.98px) {
                    .cascading-right {
                        margin-right: 0;
                    }
                }`}
                </style>

                <div className="container py-4">
                    <div className="row g-0 align-items-center">
                        <div className="col-lg-6 mb-5 mb-lg-0">
                            <div className="card cascading-right" style={{
                                background: 'hsla(0, 0%, 100%, 0.55)',
                                backdropFilter: 'blur(30px)'
                            }}>
                                <div className="card-body p-5 shadow-lg text-center">
                                    <h2 className="font-weight-bold mb-5">Login</h2>
                                    <form onSubmit={(e) => onSubmit(e)}>
                                        <div className="form-outline mb-4">
                                            <label className="form-label" htmlFor="form3Example3">Email address</label>
                                            <input
                                                type={"email"}
                                                className="form-control"
                                                id="form3Example3"
                                                placeholder="Enter your email"
                                                name="email"
                                                value={email}
                                                onChange={(e) => onInputChange(e)}
                                            />
                                        </div>

                                        <div className="form-outline mb-4">
                                            <label className="form-label" htmlFor="form3Example4">Password</label>
                                            <input
                                                type={"password"}
                                                className="form-control"
                                                id="form3Example4"
                                                placeholder="Enter your password"
                                                name="password"
                                                value={password}
                                                onChange={(e) => onInputChange(e)}
                                            />
                                        </div>

                                        <button type="submit" className="btn btn-primary btn-block mb-4 ">
                                            Sign up
                                        </button>
                                    </form>

                                    {/* Display login error */}
                                    {loginError && <p className="text-danger">{loginError}</p>}

                                    <div className="text-center">
                                        <p>or register with:</p>
                                        <NavLink to="/register" className="btn btn-outline-dark ms-auto me-2">
                                            <span className="fa fa-sign-in me-1"></span> Register
                                        </NavLink>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-6 mb-5 mb-lg-0">
                            <img src="https://mdbootstrap.com/img/new/ecommerce/vertical/004.jpg" className="w-100 rounded-4 shadow-lg" alt="" />
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
};

export default Login;
