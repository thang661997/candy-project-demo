import React, { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import axios from 'axios';

const Checkout = () => {
    let navigate = useNavigate();
    const dispatch = useDispatch();

    const location = useLocation();
    const cartItems = location.state || [];

    const getTotalPrice = () => {
        let total = 0;
        cartItems.forEach((item) => {
            total += (item.productPrice * item.quantity);
        });
        return total.toFixed(2);
    };

    const [formData, setFormData] = useState({
        namePersonal: '',
        phone: '',
        email: '',
        address: '',
        cardNumber: '',
        cvcCode: '',
    });

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormData((prevData) => ({
            ...prevData,
            [name]: value,
        }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const dataToSend = {
                form: formData,
                cartItems: cartItems,
                totalPrice: getTotalPrice(),
            };
            console.log(dataToSend);
            const response = await axios.post('http://localhost:8080/api/candy-checkout/checkout', dataToSend);
            console.log(response.data);         
            if (response.status === 200) {
                dispatch({ type: "CLEAR_CART" });
            }
            navigate("/");
        } catch (err) {
            console.log(err);
        }
    }

    return (
        <>
            <div className="container my-5">
                <form onSubmit={handleSubmit}>
                    <div className="row g-5">
                        <div className="col-md-4 order-md-2 mb-4">
                            <h4 className="d-flex justify-content-between align-items-center mb-3">
                                <span className="text-muted">Your cart</span>
                                <span className="badge badge-secondary badge-pill">{cartItems.length}</span>
                            </h4>
                            <ul className="list-group mb-3">
                                {cartItems.map((item) => (
                                    <li className="list-group-item d-flex justify-content-between lh-condensed" key={item.productID}>
                                        <div>
                                            <h6 className="my-0">{item.productName}</h6>
                                            <small className="text-muted">Quantity: {item.quantity}</small>
                                        </div>
                                        <span className="text-muted">${(item.productPrice * item.quantity).toFixed(2)}</span>
                                    </li>
                                ))}
                                <li className="list-group-item d-flex justify-content-between">
                                    <span>Total (USD)</span>
                                    <strong>${getTotalPrice()}</strong>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-8 col-lg-8">
                            <h4 className="mb-3">Billing address</h4>

                            <div className="row g-3">
                                <div className="col-sm-6">
                                    <label htmlFor="namePersonal" className="form-label">Name</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="namePersonal"
                                        name="namePersonal" // Add the 'name' attribute here
                                        placeholder=""
                                        required=""
                                        value={formData.namePersonal} // Set the value from the 'formData' state
                                        onChange={handleInputChange} // Handle input changes
                                    />

                                    <div className="invalid-feedback">
                                        Valid namePersonal is required.
                                    </div>
                                </div>

                                <div className="col-sm-6">
                                    <label htmlFor="phone" className="form-label">Phone</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="phone"
                                        name="phone" // Add the 'name' attribute here
                                        placeholder=""
                                        required=""
                                        value={formData.phone} // Set the value from the 'formData' state
                                        onChange={handleInputChange} // Handle input changes
                                    />
                                    <div className="invalid-feedback">
                                        Valid first name is required.
                                    </div>
                                </div>

                                <div className="col-12">
                                    <label htmlFor="email" className="form-label">Email</label>
                                    <div className="input-group has-validation">
                                        <span className="input-group-text">@</span>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="email"
                                            name="email" // Add the 'name' attribute here
                                            placeholder="Email"
                                            required=""
                                            value={formData.email} // Set the value from the 'formData' state
                                            onChange={handleInputChange} // Handle input changes
                                        />
                                        <div className="invalid-feedback">
                                            Your username is required.
                                        </div>
                                    </div>
                                </div>

                                <div className="col-12">
                                    <label htmlFor="address" className="form-label">Address</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="address"
                                        name="address" // Add the 'name' attribute here
                                        placeholder="1234 Main St"
                                        required=""
                                        value={formData.address} // Set the value from the 'formData' state
                                        onChange={handleInputChange} // Handle input changes
                                    />
                                    <div className="invalid-feedback">
                                        Please enter your shipping address.
                                    </div>
                                </div>
                            </div>

                            <hr className="my-4" />

                            <h4 className="mb-3">Payment</h4>

                            <div className="row gy-3">

                                <div className="col-md-6">
                                    <label htmlFor="cardNumber" className="form-label">Credit card number</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="cardNumber"
                                        name="cardNumber"
                                        placeholder=""
                                        required=""
                                        value={formData.cardNumber}
                                        onChange={handleInputChange}
                                    />
                                    <div className="invalid-feedback">
                                        Credit card number is required
                                    </div>
                                </div>

                                <div className="col-md-3">
                                    <label htmlFor="cvcCode" className="form-label">CVV</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="cvcCode"
                                        name="cvcCode"
                                        placeholder=""
                                        required=""
                                        value={formData.cvcCode}
                                        onChange={handleInputChange}
                                    />
                                    <div className="invalid-feedback">
                                        Security code required
                                    </div>
                                </div>
                            </div>

                            <hr className="my-4" />

                            <button className="w-100 btn btn-primary btn-lg" type="submit">Continue to checkout</button>
                        </div>
                    </div>
                </form>
            </div>
        </>
    )
}

export default Checkout
