import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { delItem } from '../redux/actions/index'
import { NavLink, useNavigate } from 'react-router-dom';


const Cart = () => {

    const state = useSelector((state) => state.addItem)
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const [quantity, setQuantity] = useState({});

    const handleClose = (item) => {
        dispatch(delItem(item))
    }

    const goToCheckout = () => {
        const cartData = state.map(item => ({ ...item, quantity: quantity[item.productID] || 1 }));
        navigate('/checkout', { state: cartData });
    };

    const handleDecrease = (itemId) => {
        setQuantity((prevQuantity) => {
            const newQuantity = {
                ...prevQuantity,
                [itemId]: (prevQuantity[itemId] || 1) - 1,
            }

            if (newQuantity[itemId] === 0) {
                handleClose(state.find((item) => item.productID === itemId))
            }

            return newQuantity

        });
    };

    const handleIncrease = (itemId) => {
        setQuantity((prevQuantity) => ({
            ...prevQuantity,
            [itemId]: (prevQuantity[itemId] || 0) + 1,
        }));
    };

    const getTotalPrice = () => {
        let total = 0;
        state.forEach((item) => {
            total += (item.productPrice * (quantity[item.productID] || 1));
        });
        return total.toFixed(2);
    };

    const emptyCart = () => {
        return (
            <div className="px-4 my-5 bg-light rounded-3 py-5">
                <div className="container py-4">
                    <div className="row">
                        <h3>Your Cart is Empty</h3>
                    </div>
                </div>
            </div>
        );
    }

    const summary = () => {
        return (
            <div className="col-md-4">
                <div className="card mb-4">
                    <div className="card-header py-3">
                        <h5 className="mb-0">Summary</h5>
                    </div>
                    <div className="card-body">
                        <ul className="list-group list-group-flush">
                            <li className="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
                                Products
                                <span>${getTotalPrice()}</span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center px-0">
                                Shipping
                                <span>Gratis</span>
                            </li>
                            <li className="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
                                <div>
                                    <strong>Total amount</strong>
                                    <strong>
                                        <p className="mb-0">(including VAT)</p>
                                    </strong>
                                </div>
                                <span><strong>${getTotalPrice()}</strong></span>
                            </li>
                        </ul>
                        <div className="d-flex flex-column bd-highlight" >
                            <button
                                className="btn btn-primary btn-lg btn-block"
                                onClick={goToCheckout}
                            >
                                Go to checkout
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        )
    }

    const cartItems = (cartItem) => {
        return (
            <>
                <div className="row" key={cartItem.productID}>
                    <div className="col-lg-3 col-md-12 mb-4 mb-lg-0">
                        <div className="bg-image hover-overlay hover-zoom ripple rounded" data-mdb-ripple-color="light">
                            <img src={`/${cartItem.images[0].imageName}`} className="w-100" alt={cartItem.productName} />
                            <NavLink to="#!">
                                <div className="mask" style={{ backgroundColor: "rgba(251, 251, 251, 0.2)" }}></div>
                            </NavLink>
                        </div>
                    </div>
                    <div className="col-lg-5 col-md-6 mb-4 mb-lg-0">
                        <p><strong>{cartItem.productName}</strong></p>
                        <p>Price: ${cartItem.productPrice}</p>
                        <button type="button" onClick={() => handleClose(cartItem)} className="btn btn-primary btn-sm me-1 mb-2" data-mdb-toggle="tooltip"
                            title="Remove item">
                            <i className="fa fa-trash"></i>
                        </button>
                    </div>
                    <div className="col-lg-4 col-md-6 mb-4 mb-lg-0">
                        <div className="d-flex mb-4" style={{ maxWidth: "300px" }}>
                            <button
                                className="btn btn-primary px-3 me-2"
                                onClick={() => handleDecrease(cartItem.productID)}
                            >
                                <i className="fa fa-minus"></i>
                            </button>
                            <div className="form-outline">
                                <input
                                    min="0"
                                    name="quantity"
                                    value={quantity[cartItem.productID] || 1}
                                    type="number"
                                    className="form-control"
                                    onChange={(e) => {
                                        setQuantity((prevQuantity) => ({
                                            ...prevQuantity,
                                            [cartItem.productID]: parseInt(e.target.value),
                                        }));
                                    }}
                                />
                            </div>
                            <button
                                className="btn btn-primary px-3 ms-2"
                                onClick={() => handleIncrease(cartItem.productID)}
                            >
                                <i className="fa fa-plus"></i>
                            </button>
                        </div>
                        <p className="text-start text-md-center">
                            <strong>${(cartItem.productPrice * (quantity[cartItem.productID] || 1)).toFixed(2)}</strong>
                        </p>
                    </div>
                </div>
                <hr className="my-4" />
            </>
        );
    };

    return (
        <>
            <section className="h-100 gradient-custom">
                <div className="container py-5">
                    <div className="row d-flex justify-content-center my-4">
                        <div className="col-md-8">
                            <div className="card mb-4">
                                <div className="card-header py-3">
                                    <h5 className="mb-0">Cart - {state.length} items</h5>
                                </div>
                                <div className="card-body">
                                    {state.length === 0 && emptyCart()}
                                    {state.length !== 0 && state.map(cartItems)}
                                </div>
                            </div>
                        </div>
                        {state.length !== 0 && summary()}
                    </div>
                </div>
            </section>
        </>
    )
}

export default Cart