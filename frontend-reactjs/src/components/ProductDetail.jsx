import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router'
import { useDispatch } from 'react-redux';
import { addItem, delItem } from '../redux/actions/index';
import axios from 'axios';

const ProductDetail = () => {

    const [products, setProducts] = useState([]);
    const [product, setProduct] = useState(null); // Use product state to store the selected product

    // Fetch data from the backend API using useEffect hook
    useEffect(() => {
        const fetchProducts = async () => {
            try {
                const response = await axios.get('http://localhost:8080/api/candy-home/products');
                setProducts(response.data);
            } catch (error) {
                console.error('Error fetching products:', error);
            }
        };

        fetchProducts();
    }, []);

    useEffect(() => {
        console.log(product);
    }, [product]);

    const [cartBtn, setCartBtn] = useState("Add to Cart");
    const proid = useParams();

    useEffect(() => {
        // Find the product based on the proid.id parameter when products are available
        if (products.length > 0) {
            const proDetail = products.filter((product) => product.productID === parseInt(proid.id));
            if (proDetail.length > 0) {
                setProduct(proDetail[0]);
            }
        }
    }, [products, proid.id]);

    const dispatch = useDispatch()

    const [quantity, setQuantity] = useState(1)

    const handleDecrease = () => {
        setQuantity((prevQuantity) => Math.max(1, prevQuantity - 1))
    }

    const handleIncrease = () => {
        setQuantity((prevQuantity) => Math.min(100, prevQuantity + 1))
    }

    const handleCart = (product) => {
        if (cartBtn === "Add to Cart") {
            dispatch(addItem(product)); // Pass the quantity to the addItem action
            setCartBtn("Remove from Cart")
        } else {
            dispatch(delItem(product))
            setCartBtn("Add to Cart")
        }
    }

    if (!product) {
        // If the product is still loading or not available yet
        return <div>Loading...</div>;
    }

    return (
        <>
            <div className="container my-5 py-3">
                <div className="row">
                    <div className="col-md-6 d-flex justify-content-center mx-auto product">
                        <img src={`/${product.images[0].imageName}`} alt={product.productName} height={400} />
                    </div>
                    <div className="col-md-6 d-flex flex-column justify-content-center">
                        <h1 className="display-5 fw-bold">{product.productName}</h1>
                        <hr />
                        <h2 className="my-4">${product.productPrice}</h2>
                        <p className="lead">{product.productDescription}</p>
                        <button onClick={() => handleCart(product)} className="btn btn-outline-dark my-5">{cartBtn}</button>
                        {/* <div className="d-flex align-items-center mb-4" >
                            <button
                                className="btn btn-primary px-3"
                                onClick={handleDecrease}
                            >
                                <i className="fa fa-minus"></i>
                            </button>
                            <div className="form-outline">
                                <input
                                    min="1"
                                    name="quantity"
                                    value={quantity}
                                    type="number"
                                    className="form-control"
                                    onChange={(e) => {
                                        setQuantity(parseInt(e.target.value))
                                    }}
                                />
                            </div>
                            <button
                                className="btn btn-primary px-3 me-2"
                                onClick={handleIncrease}
                            >
                                <i className="fa fa-plus"></i>
                            </button>
                            <button className="btn btn-outline-dark my-5 w-100"
                                onClick={() => handleCart(product)}>{cartBtn}</button>
                        </div> */}
                    </div>
                </div>
            </div>
        </>
    )
}

export default ProductDetail