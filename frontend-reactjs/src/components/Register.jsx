import React, { useState } from 'react'
import { useNavigate } from "react-router-dom";
import axios from 'axios';

const Register = () => {

    let navigate = useNavigate();

    const [registerError, setRegisterError] = useState('');

    const [formData, setFormData] = useState({
        namePersonal: '',
        phone: '',
        email: '',
        password: '',
        address: '',
    });

    const { namePersonal, phone, email, password, address } = formData;

    console.log(formData)

    const onInputChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const onSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.post("http://localhost:8080/api/candy-register/register", formData);
            if (response.status === 201) {
                navigate("/login");

                console.log(response)
            }
        } catch (error) {
            if (error.response && error.response.status === 401) {
                setRegisterError("Exists email")
            } else {
                setRegisterError("An error occurred")
            }
        }
    };


    return (
        <>
            <section className="text-center text-lg-start">
                <style>
                    {`.cascading-right {
                    margin-right: -50px;
                }
                
                @media (max-width: 991.98px) {
                    .cascading-right {
                        margin-right: 0;
                    }
                }`}
                </style>

                <div className="container py-4">
                    <div className="row g-0 align-items-center">
                        <div className="col-lg-6 mb-5 mb-lg-0">
                            <div className="card cascading-right" style={{
                                background: 'hsla(0, 0%, 100%, 0.55)',
                                backdropFilter: 'blur(30px)'
                            }}>
                                <div className="card-body p-5 shadow-lg text-center">
                                    <h2 className="font-weight-bold mb-5">Register</h2>
                                    <form onSubmit={(e) => onSubmit(e)}>
                                        <div className="row">
                                            <div className="col-md-6 mb-4">
                                                <div className="form-outline">
                                                    <label className="form-label" htmlFor="form3Example1">Name Personal</label>
                                                    <input
                                                        type={"text"}
                                                        className="form-control"
                                                        id="form3Example1"
                                                        placeholder="Enter your name"
                                                        name="namePersonal"
                                                        value={namePersonal}
                                                        onChange={(e) => onInputChange(e)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-6 mb-4">
                                                <div className="form-outline">
                                                    <label className="form-label" htmlFor="form3Example2">Phone</label>
                                                    <input
                                                        type={"text"}
                                                        className="form-control"
                                                        id="form3Example2"
                                                        placeholder="Enter your phone"
                                                        name="phone"
                                                        value={phone}
                                                        onChange={(e) => onInputChange(e)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-12 mb-4">
                                                <div className="form-outline">
                                                    <label className="form-label" htmlFor="form3Example3">Email Address</label>
                                                    <input
                                                        type={"email"}
                                                        className="form-control"
                                                        id="form3Example3"
                                                        placeholder="Enter your email"
                                                        name="email"
                                                        value={email}
                                                        onChange={(e) => onInputChange(e)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-6 mb-4">
                                                <div className="form-outline">
                                                    <label className="form-label" htmlFor="form3Example4">Password</label>
                                                    <input
                                                        type={"password"}
                                                        className="form-control"
                                                        id="form3Example4"
                                                        placeholder="Enter your password"
                                                        name="password"
                                                        value={password}
                                                        onChange={(e) => onInputChange(e)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-6 mb-4">
                                                <div className="form-outline">
                                                    <label className="form-label" htmlFor="form3Example5">Re - Password</label>
                                                    <input
                                                        type={"password"}
                                                        className="form-control"
                                                        id="form3Example5"
                                                        placeholder="Enter your re-password"
                                                        name="re-password"
                                                        onChange={(e) => onInputChange(e)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-12 mb-4">
                                                <label className="form-label" htmlFor="form3Example6">Address</label>
                                                <input
                                                        type={"text"}
                                                        className="form-control"
                                                        id="form3Example6"
                                                        placeholder="Enter your address"
                                                        name="address"
                                                        value={address}
                                                        onChange={(e) => onInputChange(e)}
                                                    />
                                            </div>
                                        </div>

                                        <button type="submit" className="btn btn-primary btn-block mb-4 ">
                                            Register
                                        </button>

                                    </form>
                                    {/* Display register error */}
                                    {registerError && <p className="text-danger">{registerError}</p>}
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-6 mb-5 mb-lg-0">
                            <img src="https://mdbootstrap.com/img/new/ecommerce/vertical/004.jpg" className="w-100 rounded-4 shadow-lg" alt="" />
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Register