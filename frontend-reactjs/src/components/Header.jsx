import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import Login from './buttons/Login'
import Signup from './buttons/Signup'
import CartBtn from './buttons/CartBtn'
import axios from 'axios';

const Header = () => {

    const [userNamePersonal, setUserNamePersonal] = useState('');

    useEffect(() => {
        const storedNamePersonal = sessionStorage.getItem('namePersonal');
        if (storedNamePersonal) {
            console.log('Setting userNamePersonal:', storedNamePersonal);
            setUserNamePersonal(storedNamePersonal);
        }
    }, []);

    console.log('Rendering Header with userNamePersonal:', userNamePersonal);

    const handleLogout = () => {
        // Make an API call to logout, if necessary
        axios.post('http://localhost:8080/api/candy-login/logout')
            .then((response) => {
                if (response.status === 200) {
                    // Clear session storage and reset state
                    sessionStorage.removeItem('namePersonal');
                    setUserNamePersonal('');
                }
            })
            .catch((error) => {
                console.error('Error logging out:', error);
            });
    };

    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-white py-3 shadow-sm">
                <div className="container">
                    <NavLink className="navbar-brand fw-bold fs-4" to="/">CANDY MIUSUMI</NavLink>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mx-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/products">Products</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/about">About</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/contact">Contact</NavLink>
                            </li>
                        </ul>
                        <div className="buttons d-flex">
                            {userNamePersonal ? (
                                <div className="dropdown">
                                    <button
                                        className="btn btn-outline-dark ms-auto dropdown-toggle"
                                        type="button"
                                        id="userDropdown"
                                        data-bs-toggle="dropdown"
                                        aria-expanded="false"
                                    >
                                        Welcome, {userNamePersonal}
                                    </button>
                                    <ul className="dropdown-menu dropdown-menu-end" aria-labelledby="userDropdown">
                                        <li>
                                            <button className="dropdown-item" onClick={handleLogout}>
                                                Logout
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            ) : (
                                <>
                                    <NavLink to="/login" className="btn btn-outline-dark ms-auto me-2">
                                        <span className="fa fa-sign-in me-1"></span> Login
                                    </NavLink>
                                    <NavLink to="/register" className="btn btn-outline-dark ms-auto me-2">
                                        <span className="fa fa-sign-in me-1"></span> Register
                                    </NavLink>
                                </>
                            )}
                            
                            <CartBtn />
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    )
}

export default Header