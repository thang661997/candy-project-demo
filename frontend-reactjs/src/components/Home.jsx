import React, { useState, useEffect } from 'react'
import Product from './Product'
import axios from 'axios';

const Home = () => {

    const [banners, setBanners] = useState([]);

    // Fetch data from the backend API using useEffect hook
    useEffect(() => {
        const fetchBanners = async () => {
            try {
                const response = await axios.get('http://localhost:8080/api/candy-banner/banners');
                setBanners(response.data);
                console.log(response.data)
            } catch (error) {
                console.error('Error fetching products:', error);
            }
        };

        fetchBanners();
    }, []);

    return (
        <div>
            <div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="carousel">
                <div className="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 4"></button>
                </div>
                <div className="carousel-inner">
                    {banners.map(banner => (
                        <div className="carousel-item active">
                            <img key={banner.bannerID} src={`/${banner.bannerName}`} alt={banner.bannerName} className="d-block w-100" height={643} />
                        </div>
                    ))}
                </div>
                <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Previous</span>
                </button>
                <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Next</span>
                </button>
            </div>
            <Product />
        </div>
    )
}

export default Home