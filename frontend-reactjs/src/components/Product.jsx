import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom'
import axios from 'axios';

const Product = () => {

    const [products, setProducts] = useState([]);

    // Fetch data from the backend API using useEffect hook
    useEffect(() => {
        const fetchProducts = async () => {
            try {
                const response = await axios.get('http://localhost:8080/api/candy-home/products');
                setProducts(response.data);
                console.log(response.data)
            } catch (error) {
                console.error('Error fetching products:', error);
            }
        };

        fetchProducts();
    }, []);

    const cardItem = (item) => {
        return (
            <>
                <div className="col">
                    <div className="card h-100" key={item.productID} >
                        <img src={item.images[0].imageName} className="card-img-top" alt={item.productName} height={250} />
                        <div className="card-body">
                            <h5 className="card-title">{item.productName}</h5>
                            <p className="card-text">
                                ${item.productPrice}
                            </p>
                        </div>
                        <div className="card-footer d-flex justify-content-center">
                            <small className="text-muted">
                                <NavLink to={`/products/${item.productID}`} className="btn btn-outline-dark">View</NavLink>
                            </small>
                        </div>
                    </div>
                </div>
            </>
        )
    }

    return (
        <div>
            <div className="container py-5">
                <div className="row">
                    <div className="col-12 text-center">
                        <h1 className="display-6 fw-bolder text-center">Product</h1>
                        <hr />
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row row-cols-1 row-cols-md-4 g-4">
                    {/* {DATA.map(cardItem)} */}
                    {products.map(cardItem)} {/* Map through the fetched products */}
                </div>
            </div>
        </div>
    )
}

export default Product