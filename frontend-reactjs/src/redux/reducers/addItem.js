const addItem = [];

const addItems = (state = addItem, action) => {
    switch (action.type) {
        case "ADDITEM":
            return [
                ...state,
                action.payload // Add a new property "quantity" with the initial value of 1
            ]

        case "DELITEM":
            return state = state.filter((x) => {
                return x.productID !== action.payload.productID
            })

        case "CLEAR_CART":
            return [];

        default:
            return state;
    }
}

export default addItems