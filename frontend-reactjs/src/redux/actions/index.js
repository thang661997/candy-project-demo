export const addItem = (product) => {
    return {
        type: "ADDITEM",
        payload: product // Include the quantity in the payload
    }
}

export const delItem = (product) => {
    return {
        type: "DELITEM",
        payload: product
    }
}
