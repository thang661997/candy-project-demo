const DATA = [
    {
        id : 0,
        category : "Chocolates",
        title : "Peanut Butter Cups",
        price : 750,
        desc : "With their rich, slightly crunchy chocolate shells and their creamy peanut butter centers, these homemade peanut butter cups are even better than the ones that come in the orange pack.",
        img : "/assets/images/category/chocolates/peanut-butter-cups-1.jpg"
    },
    {
        id : 1,
        category : "Chocolates",
        title : "Chocolate Mounds Candy",
        price : 850,
        desc : "The chocolate outsides surround sweet, sugared coconut centers. If you’re looking for a homemade candy that tastes just like the candy bar it’s named for, these are the treats for you.",
        img : "/assets/images/category/chocolates/Coconut-Candy-1.jpg"
    },
    {
        id : 2,
        category : "Chocolates",
        title : "Peanut Butter Truffles",
        price : 950,
        desc : "The white chocolate is delicate and light, and the hidden Rice Krispies add an airy bit of crunch, but the thick peanut butter filling is surprisingly hearty and 100% delicious.",
        img : "/assets/images/category/chocolates/white-chocolate-and-coconut-truffles.jpg"
    },
    {
        id : 3,
        category : "Chocolates",
        title : "Chocolate Peanut Clusters",
        price : 1050,
        desc : "Chocolate peanut clusters are the ideal combination of salty and sweet, and crunchy and smooth. They’re also effortless to make.",
        img : "/assets/images/category/chocolates/Chocolate-Peanut-Clusters-1.jpg"
    },
    {
        id : 4,
        category : "Gummi",
        title : "Gummi Bunnies",
        price : 1150,
        desc : "Gummi Easter bunnies for your Easter basket or Easter buffet!  Sweet pastels in Lemon, Orange, Pineapple, Strawberry, Green Apple & Grape flavors.",
        img : "/assets/images/category/gummi/Gummi-Bunnies-1.jpg"
    },
    {
        id : 5,
        category : "Gummi",
        title : "Jelly Belly Sour Pucker Lips",
        price : 1250,
        desc : "Jelly Belly Schmoochi Lips are a burst of sour first, and end with sweet goodness.  Red and Pink Lips, are cherry and pink lemonade flavored.",
        img : "/assets/images/category/gummi/Jelly-Belly-Sour-Pucker-Lips.jpg"
    },
    {
        id : 6,
        category : "Gummi",
        title : "Haribo Gummi Fruit Salad",
        price : 1350,
        desc : "Naturally flavored with balanced sweetness, Haribo Gummi Fruit Salad is mouth-watering gummi candy and are fat-free! Fruit shapes includes lime, grapefruit, lemon, orange, cherry and passion fruit flavors.",
        img : "/assets/images/category/gummi/Gummi-Fruit-Salad.jpg"
    },
    {
        id : 7,
        category : "Gummi",
        title : "Warheads Sour Chewy Cubes",
        price : 1450,
        desc : "Warheads sour cubes are stackable, snackable fruit chews are sour on the outside with a chewy, gummy center.  Six pucker flavors...  Orange, Green Apple, Strawberry, Blue Raspberry, Watermelon and Black Cherry.",
        img : "/assets/images/category/gummi/Sour-Chewy-Cubes.jpg"
    },
]

export default DATA;