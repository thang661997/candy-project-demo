/**
 * @author ThangHT8
 * @date 31 Jul 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.enums;

public enum RoleEnum {
	Admin,
    Customer;
}
