/**
 * @author ThangHT8
 * @date 31 Jul 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fa.training.DemoSpringBoot.entities.Images;

@Repository
public interface ImagesRepository extends JpaRepository<Images, Integer> {

	// Method to find images with productID as NULL
	List<Images> findByProductIsNull();
}
