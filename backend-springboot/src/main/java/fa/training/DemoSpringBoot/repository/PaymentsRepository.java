/**
 * @author ThangHT8
 * @date 31 Jul 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fa.training.DemoSpringBoot.entities.Payments;

@Repository
public interface PaymentsRepository extends JpaRepository<Payments, Integer>{

}
