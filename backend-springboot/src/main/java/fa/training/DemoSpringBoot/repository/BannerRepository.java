/**
 * @author ThangHT8
 * @date 2 Aug 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fa.training.DemoSpringBoot.entities.Banner;

@Repository
public interface BannerRepository extends JpaRepository<Banner, Integer>{

}
