/**
 * @author ThangHT8
 * @date 31 Jul 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.entities;

import java.util.Date;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "FeedBack")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FeedBack {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer feedBackID;

	@Column(nullable = false)
	private String content;

	@Column(nullable = false)
	private Date createDate;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "accountID", referencedColumnName = "accountID")
	private Accounts account;
}
