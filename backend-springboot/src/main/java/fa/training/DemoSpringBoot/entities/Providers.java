/**
 * @author ThangHT8
 * @date 31 Jul 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "Providers")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Providers {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer providerID;

    @Column(nullable = false)
    private String providerName;
}
