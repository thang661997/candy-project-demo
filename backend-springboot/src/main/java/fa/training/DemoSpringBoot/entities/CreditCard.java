/**
 * @author ThangHT8
 * @date 31 Jul 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "CreditCard")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CreditCard {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer creditCardID;

	@Column(nullable = false)
	private String namePersonal;

	@Column(nullable = false, length = 12)
	private String cardNumber;

	@Column(nullable = false)
	private int expMonth;

	@Column(nullable = false)
	private int expYear;

	@Column(nullable = false)
	private int cvcCode;

	@Column(nullable = false)
	private double balance;

	@Column(nullable = false)
	private String statusCreditCard;
}
