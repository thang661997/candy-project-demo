/**
 * @author ThangHT8
 * @date 31 Jul 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.entities;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "Accounts")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Accounts {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer accountID;

	@Column(nullable = true)
	private String email;
	
    @Column(name = "[password]", nullable = true)
    private String password;
    
    @Column(nullable = true)
	private String namePersonal;

    @Column(nullable = true)
	private String address;

    @Column(nullable = true)
	private String phone;

    @Column(nullable = true)
    private String statusAccount;

    @Column(nullable = true)
    private Date createDate;

	
}
