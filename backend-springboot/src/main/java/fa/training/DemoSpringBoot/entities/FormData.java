/**
 * @author ThangHT8
 * @date 4 Aug 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.entities;

import lombok.Data;

@Data
public class FormData {
	private String namePersonal;
	private String phone;
	private String email;
	private String address;
	private String cardNumber;
	private String cvcCode;
	
	
}
