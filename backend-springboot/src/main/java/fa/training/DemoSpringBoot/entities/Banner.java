/**
 * @author ThangHT8
 * @date 2 Aug 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "Banner")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Banner {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer bannerID;

	@Column(nullable = false)
	private String bannerName;

}
