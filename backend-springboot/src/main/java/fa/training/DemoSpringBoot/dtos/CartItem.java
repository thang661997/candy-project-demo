/**
 * @author ThangHT8
 * @date 4 Aug 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.dtos;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class CartItem {
	private String productName;
    private int quantity;
    private double productPrice;
}
