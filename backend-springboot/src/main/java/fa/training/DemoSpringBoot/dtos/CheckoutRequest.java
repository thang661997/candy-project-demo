/**
 * @author ThangHT8
 * @date 4 Aug 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.dtos;

import java.util.List;

import org.springframework.stereotype.Component;

import fa.training.DemoSpringBoot.entities.FormData;
import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
public class CheckoutRequest {
	private FormData form;
	private List<CartItem> cartItems;
	private Double totalPrice;
	@Override
	public String toString() {
		return "CheckoutRequest [formData=" + form + ", cartItems=" + cartItems + ", totalPrice=" + totalPrice
				+ "]";
	}
	
	
}
