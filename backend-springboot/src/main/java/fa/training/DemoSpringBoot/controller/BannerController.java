/**
 * @author ThangHT8
 * @date 2 Aug 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fa.training.DemoSpringBoot.entities.Banner;
import fa.training.DemoSpringBoot.services.BannerService;

@RestController
@CrossOrigin
@RequestMapping("/api/candy-banner")
public class BannerController {

	@Autowired
	private BannerService bannerService;

	@GetMapping("/banners")
	public List<Banner> getAllBanner() {
		return bannerService.getAllBanner();
	}

}
