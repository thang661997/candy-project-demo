/**
 * @author ThangHT8
 * @date 3 Aug 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fa.training.DemoSpringBoot.entities.Accounts;
import fa.training.DemoSpringBoot.services.AccountsService;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/candy-register")
public class RegisterController {

	@Autowired
	private AccountsService accountService;

	@PostMapping("/register")
	public ResponseEntity<?> newAccount(@RequestBody Accounts newAccount) {

		boolean checkEmail = accountService.doesEmailExist(newAccount.getEmail());

		if (checkEmail) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Email already exists");
		} else {
			newAccount.setStatusAccount("Active");
			newAccount.setCreateDate(new Date());
			Accounts savedAccount = accountService.saveNewAccount(newAccount);
			return ResponseEntity.status(HttpStatus.CREATED).body(savedAccount);
		}

	}
}
