/**
 * @author ThangHT8
 * @date 3 Aug 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fa.training.DemoSpringBoot.entities.Accounts;
import fa.training.DemoSpringBoot.services.AccountsService;
import jakarta.servlet.http.HttpSession;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/candy-login")
public class LoginController {

	@Autowired
	private AccountsService accountService;

	@PostMapping("/login")
	public ResponseEntity<String> loginAccounts(@RequestBody Accounts accounts, HttpSession session) {

		boolean checkAccount = accountService.doesAccountExist(accounts.getEmail(), accounts.getPassword());

		if (checkAccount) {
			session.setAttribute("emailSession", accounts.getEmail());
			return ResponseEntity.ok().body("Success");
		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid credentials");
		}
	}

	@PostMapping("/logout")
	public ResponseEntity<String> logout() {
		try {
			return ResponseEntity.ok().body("Logged out successfully");
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error during logout");
		}
	}
}
