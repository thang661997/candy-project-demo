/**
 * @author ThangHT8
 * @date 4 Aug 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fa.training.DemoSpringBoot.dtos.CheckoutRequest;
import fa.training.DemoSpringBoot.services.CheckoutService;
import jakarta.servlet.http.HttpSession;

@RestController
@CrossOrigin
@RequestMapping("/api/candy-checkout")
public class CheckoutController {

	@Autowired
	private CheckoutService checkoutService;

	@PostMapping("/checkout")
	public ResponseEntity<String> checkout(@RequestBody CheckoutRequest checkoutRequest, HttpSession session) {
		System.out.println(checkoutRequest);
		try {
			checkoutService.processCheckout(checkoutRequest, session);
			return ResponseEntity.ok("Checkout successful!");
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error during checkout.");
		}
	}
}
