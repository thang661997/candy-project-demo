/**
 * @author ThangHT8
 * @date 31 Jul 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.services.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fa.training.DemoSpringBoot.services.CreditCardService;

@Service
@Transactional
public class CreditCardServiceImpl implements CreditCardService {

}
