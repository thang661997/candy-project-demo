/**
 * @author ThangHT8
 * @date 31 Jul 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fa.training.DemoSpringBoot.entities.Images;
import fa.training.DemoSpringBoot.repository.ImagesRepository;
import fa.training.DemoSpringBoot.services.ImagesService;

@Service
@Transactional
public class ImagesServiceImpl implements ImagesService {

	@Autowired
    private ImagesRepository imagesRepository;

    // Method to fetch images with productID as NULL
    public List<Images> getImagesWithNullProductID() {
        return imagesRepository.findByProductIsNull();
    }
}
