/**
 * @author ThangHT8
 * @date 4 Aug 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.services.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.training.DemoSpringBoot.dtos.CartItem;
import fa.training.DemoSpringBoot.dtos.CheckoutRequest;
import fa.training.DemoSpringBoot.entities.Accounts;
import fa.training.DemoSpringBoot.entities.CreditCard;
import fa.training.DemoSpringBoot.entities.FormData;
import fa.training.DemoSpringBoot.entities.OrderDetails;
import fa.training.DemoSpringBoot.entities.Orders;
import fa.training.DemoSpringBoot.entities.Payments;
import fa.training.DemoSpringBoot.entities.Product;
import fa.training.DemoSpringBoot.repository.AccountsRepository;
import fa.training.DemoSpringBoot.repository.CreditCardRepository;
import fa.training.DemoSpringBoot.repository.OrderDetailsRepository;
import fa.training.DemoSpringBoot.repository.OrdersRepository;
import fa.training.DemoSpringBoot.repository.PaymentsRepository;
import fa.training.DemoSpringBoot.repository.ProductRepository;
import fa.training.DemoSpringBoot.services.CheckoutService;
import jakarta.servlet.http.HttpSession;
import jakarta.transaction.Transactional;

@Service
@Transactional
public class CheckoutServiceImpl implements CheckoutService {

	@Autowired
	private AccountsRepository accountsRepo;

	@Autowired
	private OrdersRepository ordersRepo;

	@Autowired
	private OrderDetailsRepository orderDetailsRepo;

	@Autowired
	private PaymentsRepository paymentsRepo;

	@Autowired
	private CreditCardRepository creditCardRepo;

	@Autowired
	private ProductRepository productRepo;

	@Override
	public void processCheckout(CheckoutRequest checkoutRequest, HttpSession session) {

		// Create and save Accounts entity

		FormData formData = checkoutRequest.getForm();
		
//		String email = (String) session.getAttribute("emailSession");

		Accounts account = null;
		Accounts accountByEmail = null;

		boolean existsAccount = accountsRepo.existsByEmail(formData.getEmail());

		if (existsAccount) {
			accountByEmail = accountsRepo.findByEmail(formData.getEmail());
		} else {
			account = new Accounts();
			account.setNamePersonal(formData.getNamePersonal());
			account.setAddress(formData.getAddress());
			account.setPhone(formData.getPhone());
			account.setEmail(formData.getEmail());
			account.setPassword(null);
			account.setStatusAccount("Active");
			account.setCreateDate(new Date());
			accountsRepo.save(account);
		}

		// Create and save Orders entity
		Orders order = new Orders();
		order.setOrderDate(new Date());
		order.setStatusOrder("Pending");
		if (existsAccount) {
			order.setAccount(accountByEmail);
		} else {
			order.setAccount(account);
		}
		ordersRepo.save(order);

		// Create and save OrderDetails entities
		for (CartItem cartItem : checkoutRequest.getCartItems()) {
			OrderDetails orderDetails = new OrderDetails();
			orderDetails.setQuantity(cartItem.getQuantity());
			orderDetails.setUnitPrice(cartItem.getProductPrice());
			orderDetails.setDiscount(0);
			orderDetails.setOrder(order);

			Product product = productRepo.findByProductName(cartItem.getProductName());
			orderDetails.setProduct(product);
			orderDetailsRepo.save(orderDetails);
		}

		// Create and save Payments entity
		Payments payment = new Payments();
		payment.setAmount(checkoutRequest.getTotalPrice());
		payment.setPaymentDate(new Date());
		payment.setOrder(order);

		// Check if the credit card number exists in the database
		String cardNumber = checkoutRequest.getForm().getCardNumber();
		CreditCard existingCreditCard = creditCardRepo.findByCardNumber(cardNumber);

		if (existingCreditCard != null) {
			// Update balance of existing credit card
			existingCreditCard.setBalance(existingCreditCard.getBalance() - checkoutRequest.getTotalPrice());
			creditCardRepo.save(existingCreditCard);
			payment.setCreditCard(existingCreditCard);
		} else {
			// Create and save CreditCard entity
			CreditCard creditCard = new CreditCard();
			creditCard.setNamePersonal(checkoutRequest.getForm().getNamePersonal());
			creditCard.setCardNumber(checkoutRequest.getForm().getCardNumber());
			creditCard.setCvcCode(Integer.parseInt(checkoutRequest.getForm().getCvcCode()));
			creditCard.setBalance(checkoutRequest.getTotalPrice()); // Assuming initial balance is same as payment amount
			creditCard.setStatusCreditCard("Active");
			creditCardRepo.save(creditCard);
			payment.setCreditCard(creditCard);
		}

		paymentsRepo.save(payment);
	}

}
