/**
 * @author ThangHT8
 * @date 31 Jul 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.services;

import java.util.List;

import fa.training.DemoSpringBoot.entities.Product;

public interface ProductService {
	List<Product> getAllProducts();

}
