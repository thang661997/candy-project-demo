/**
 * @author ThangHT8
 * @date 2 Aug 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fa.training.DemoSpringBoot.entities.Banner;
import fa.training.DemoSpringBoot.repository.BannerRepository;
import fa.training.DemoSpringBoot.services.BannerService;

@Service
@Transactional
public class BannerServiceImpl implements BannerService{
	
	@Autowired
	private BannerRepository bannerRepo;

	@Override
	public List<Banner> getAllBanner() {
		return bannerRepo.findAll();
	}

}
