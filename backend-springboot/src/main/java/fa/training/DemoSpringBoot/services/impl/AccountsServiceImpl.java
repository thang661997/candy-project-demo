/**
 * @author ThangHT8
 * @date 31 Jul 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fa.training.DemoSpringBoot.entities.Accounts;
import fa.training.DemoSpringBoot.repository.AccountsRepository;
import fa.training.DemoSpringBoot.services.AccountsService;

@Service
@Transactional
public class AccountsServiceImpl implements AccountsService {

	@Autowired
	private AccountsRepository accountsRepo;

	@Override
	public Accounts saveNewAccount(Accounts newAccount) {
		return accountsRepo.save(newAccount);
	}

	@Override
	public boolean doesAccountExist(String email, String password) {
		return accountsRepo.existsByEmailAndPassword(email, password);
	}

	@Override
	public boolean doesEmailExist(String email) {
		return accountsRepo.existsByEmail(email);
	}	

}
