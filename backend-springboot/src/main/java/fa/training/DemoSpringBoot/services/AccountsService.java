/**
 * @author ThangHT8
 * @date 31 Jul 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.services;

import fa.training.DemoSpringBoot.entities.Accounts;

public interface AccountsService {

	Accounts saveNewAccount(Accounts newAccount);

	boolean doesAccountExist(String email, String password);

	boolean doesEmailExist(String email);

}
