/**
 * @author ThangHT8
 * @date 2 Aug 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.services;

import java.util.List;

import fa.training.DemoSpringBoot.entities.Banner;

public interface BannerService {

	List<Banner> getAllBanner();
}
