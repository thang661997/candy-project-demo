/**
 * @author ThangHT8
 * @date 4 Aug 2023
 * @version 1.0
 */

package fa.training.DemoSpringBoot.services;

import fa.training.DemoSpringBoot.dtos.CheckoutRequest;
import jakarta.servlet.http.HttpSession;

public interface CheckoutService {

	void processCheckout(CheckoutRequest checkoutRequest, HttpSession session);
}
